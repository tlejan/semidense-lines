# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/thomas/semidense-lines/Examples/Monocular/mono_kitti.cc" "/home/thomas/semidense-lines/build/CMakeFiles/mono_kitti.dir/Examples/Monocular/mono_kitti.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "CGAL_USE_GMP"
  "CGAL_USE_MPFR"
  "COMPILEDWITHC11"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/include/x86_64-linux-gnu"
  "."
  "../"
  "../include"
  "/usr/local/include/eigen3"
  "/home/thomas/Pangolin/include"
  "/home/thomas/Pangolin/build/src/include"
  "../Thirdparty/Line3Dpp"
  "../Thirdparty/Line3Dpp/build"
  "../Thirdparty/EDTest"
  "/usr/local/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/thomas/semidense-lines/build/CMakeFiles/ORB_SLAM2.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
