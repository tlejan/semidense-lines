# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/thomas/semidense-lines/src/CARV/Exception.cpp" "/home/thomas/semidense-lines/build/CMakeFiles/ORB_SLAM2.dir/src/CARV/Exception.cpp.o"
  "/home/thomas/semidense-lines/src/CARV/FreespaceDelaunayAlgorithm.cc" "/home/thomas/semidense-lines/build/CMakeFiles/ORB_SLAM2.dir/src/CARV/FreespaceDelaunayAlgorithm.cc.o"
  "/home/thomas/semidense-lines/src/CARV/GraphWrapper_Boost.cc" "/home/thomas/semidense-lines/build/CMakeFiles/ORB_SLAM2.dir/src/CARV/GraphWrapper_Boost.cc.o"
  "/home/thomas/semidense-lines/src/CARV/Matrix.cc" "/home/thomas/semidense-lines/build/CMakeFiles/ORB_SLAM2.dir/src/CARV/Matrix.cc.o"
  "/home/thomas/semidense-lines/src/CARV/SFMTranscript.cpp" "/home/thomas/semidense-lines/build/CMakeFiles/ORB_SLAM2.dir/src/CARV/SFMTranscript.cpp.o"
  "/home/thomas/semidense-lines/src/CARV/SFMTranscriptInterface_Delaunay.cpp" "/home/thomas/semidense-lines/build/CMakeFiles/ORB_SLAM2.dir/src/CARV/SFMTranscriptInterface_Delaunay.cpp.o"
  "/home/thomas/semidense-lines/src/CARV/SFMTranscriptInterface_ORBSLAM.cpp" "/home/thomas/semidense-lines/build/CMakeFiles/ORB_SLAM2.dir/src/CARV/SFMTranscriptInterface_ORBSLAM.cpp.o"
  "/home/thomas/semidense-lines/src/CARV/StringFunctions.cpp" "/home/thomas/semidense-lines/build/CMakeFiles/ORB_SLAM2.dir/src/CARV/StringFunctions.cpp.o"
  "/home/thomas/semidense-lines/src/CARV/lovimath.cc" "/home/thomas/semidense-lines/build/CMakeFiles/ORB_SLAM2.dir/src/CARV/lovimath.cc.o"
  "/home/thomas/semidense-lines/src/Converter.cc" "/home/thomas/semidense-lines/build/CMakeFiles/ORB_SLAM2.dir/src/Converter.cc.o"
  "/home/thomas/semidense-lines/src/Frame.cc" "/home/thomas/semidense-lines/build/CMakeFiles/ORB_SLAM2.dir/src/Frame.cc.o"
  "/home/thomas/semidense-lines/src/FrameDrawer.cc" "/home/thomas/semidense-lines/build/CMakeFiles/ORB_SLAM2.dir/src/FrameDrawer.cc.o"
  "/home/thomas/semidense-lines/src/Initializer.cc" "/home/thomas/semidense-lines/build/CMakeFiles/ORB_SLAM2.dir/src/Initializer.cc.o"
  "/home/thomas/semidense-lines/src/KeyFrame.cc" "/home/thomas/semidense-lines/build/CMakeFiles/ORB_SLAM2.dir/src/KeyFrame.cc.o"
  "/home/thomas/semidense-lines/src/KeyFrameDatabase.cc" "/home/thomas/semidense-lines/build/CMakeFiles/ORB_SLAM2.dir/src/KeyFrameDatabase.cc.o"
  "/home/thomas/semidense-lines/src/LineDetector.cc" "/home/thomas/semidense-lines/build/CMakeFiles/ORB_SLAM2.dir/src/LineDetector.cc.o"
  "/home/thomas/semidense-lines/src/LocalMapping.cc" "/home/thomas/semidense-lines/build/CMakeFiles/ORB_SLAM2.dir/src/LocalMapping.cc.o"
  "/home/thomas/semidense-lines/src/LoopClosing.cc" "/home/thomas/semidense-lines/build/CMakeFiles/ORB_SLAM2.dir/src/LoopClosing.cc.o"
  "/home/thomas/semidense-lines/src/Map.cc" "/home/thomas/semidense-lines/build/CMakeFiles/ORB_SLAM2.dir/src/Map.cc.o"
  "/home/thomas/semidense-lines/src/MapDrawer.cc" "/home/thomas/semidense-lines/build/CMakeFiles/ORB_SLAM2.dir/src/MapDrawer.cc.o"
  "/home/thomas/semidense-lines/src/MapPoint.cc" "/home/thomas/semidense-lines/build/CMakeFiles/ORB_SLAM2.dir/src/MapPoint.cc.o"
  "/home/thomas/semidense-lines/src/Modeler.cc" "/home/thomas/semidense-lines/build/CMakeFiles/ORB_SLAM2.dir/src/Modeler.cc.o"
  "/home/thomas/semidense-lines/src/ORBextractor.cc" "/home/thomas/semidense-lines/build/CMakeFiles/ORB_SLAM2.dir/src/ORBextractor.cc.o"
  "/home/thomas/semidense-lines/src/ORBmatcher.cc" "/home/thomas/semidense-lines/build/CMakeFiles/ORB_SLAM2.dir/src/ORBmatcher.cc.o"
  "/home/thomas/semidense-lines/src/Optimizer.cc" "/home/thomas/semidense-lines/build/CMakeFiles/ORB_SLAM2.dir/src/Optimizer.cc.o"
  "/home/thomas/semidense-lines/src/PnPsolver.cc" "/home/thomas/semidense-lines/build/CMakeFiles/ORB_SLAM2.dir/src/PnPsolver.cc.o"
  "/home/thomas/semidense-lines/src/ProbabilityMapping.cc" "/home/thomas/semidense-lines/build/CMakeFiles/ORB_SLAM2.dir/src/ProbabilityMapping.cc.o"
  "/home/thomas/semidense-lines/src/Sim3Solver.cc" "/home/thomas/semidense-lines/build/CMakeFiles/ORB_SLAM2.dir/src/Sim3Solver.cc.o"
  "/home/thomas/semidense-lines/src/System.cc" "/home/thomas/semidense-lines/build/CMakeFiles/ORB_SLAM2.dir/src/System.cc.o"
  "/home/thomas/semidense-lines/src/Tracking.cc" "/home/thomas/semidense-lines/build/CMakeFiles/ORB_SLAM2.dir/src/Tracking.cc.o"
  "/home/thomas/semidense-lines/src/Viewer.cc" "/home/thomas/semidense-lines/build/CMakeFiles/ORB_SLAM2.dir/src/Viewer.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "CGAL_USE_GMP"
  "CGAL_USE_MPFR"
  "COMPILEDWITHC11"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/include/x86_64-linux-gnu"
  "."
  "../"
  "../include"
  "/usr/local/include/eigen3"
  "/home/thomas/Pangolin/include"
  "/home/thomas/Pangolin/build/src/include"
  "../Thirdparty/Line3Dpp"
  "../Thirdparty/Line3Dpp/build"
  "../Thirdparty/EDTest"
  "/usr/local/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
