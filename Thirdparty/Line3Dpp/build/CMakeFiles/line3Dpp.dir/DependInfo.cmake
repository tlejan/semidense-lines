# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/thomas/semidense-lines/Thirdparty/Line3Dpp/clustering.cc" "/home/thomas/semidense-lines/Thirdparty/Line3Dpp/build/CMakeFiles/line3Dpp.dir/clustering.cc.o"
  "/home/thomas/semidense-lines/Thirdparty/Line3Dpp/line3D.cc" "/home/thomas/semidense-lines/Thirdparty/Line3Dpp/build/CMakeFiles/line3Dpp.dir/line3D.cc.o"
  "/home/thomas/semidense-lines/Thirdparty/Line3Dpp/optimization.cc" "/home/thomas/semidense-lines/Thirdparty/Line3Dpp/build/CMakeFiles/line3Dpp.dir/optimization.cc.o"
  "/home/thomas/semidense-lines/Thirdparty/Line3Dpp/sparsematrix.cc" "/home/thomas/semidense-lines/Thirdparty/Line3Dpp/build/CMakeFiles/line3Dpp.dir/sparsematrix.cc.o"
  "/home/thomas/semidense-lines/Thirdparty/Line3Dpp/view.cc" "/home/thomas/semidense-lines/Thirdparty/Line3Dpp/build/CMakeFiles/line3Dpp.dir/view.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "_FORCE_INLINES"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "."
  "/usr/local/include"
  "/usr/local/include/eigen3"
  "/usr/local/include/eigen3/unsupported"
  "/usr/local/include/eigen3/eigen3"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
