# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/thomas/semidense-lines/Thirdparty/Line3Dpp/main_bundler.cpp" "/home/thomas/semidense-lines/Thirdparty/Line3Dpp/build/CMakeFiles/runLine3Dpp_bundler.dir/main_bundler.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "_FORCE_INLINES"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "."
  "/usr/local/include"
  "/usr/local/include/eigen3"
  "/usr/local/include/eigen3/unsupported"
  "/usr/local/include/eigen3/eigen3"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/thomas/semidense-lines/Thirdparty/Line3Dpp/build/CMakeFiles/line3Dpp.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
